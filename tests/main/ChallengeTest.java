package main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChallengeTest {

    Challenge c = new Challenge();

    @Test
    void encryptTest() {
        assertEquals("khoor", c.encrypt("hello", 3));
        assertEquals("khoor zruog!", c.encrypt("Hello World!", 3));
        assertEquals("hello world!", c.encrypt("Khoor Zruog!", -3));
    }

    @Test
    void decryptTest(){
        assertEquals("hello", c.decrypt("khoor", 3));
        assertEquals("hello world!", c.decrypt("Khoor Zruog!", 3));
        assertEquals("khoor zruog!", c.decrypt("Hello World!", -3));
    }
}